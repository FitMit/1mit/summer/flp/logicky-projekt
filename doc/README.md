Logický projekt FLP 2021/2022
===============
Autor: Matěj Kudera <br>
Zadání: Kostra grafu <br>
Dokument obsahuje stručný popis činnosti logického projektu do předmětu FLP.

Použití
---
Program se spouští bez argumentů ve formátu:
```
flp21-log < [vstupni_soubor]
```
Vstup je vždy načítán ze standartního vstupu a výsledek vypočítaný programem je vždy vypisován na standartní výstup.

Implementace
---
Činnost programu začíná čtením vstupního souboru zadaného na standartní vstup. Vstupní soubor se čte po znacích a každý řádek je uložen do vlastního seznamu. Tyto seznamy jsou pak uloženy do jednoho velkého seznamu, který obsahuje všechny načtené řádky. Predikáty použíté k načtení vstupu byly převzaty z ukázkového programu v datovém skladu předmětu FLP.

Načtený soubor je dále kontrolován na správný formát vstupu. Očekává se, že řádek se správně zadanou hranou grafu bude mít formát "V1 V2", kde V1 a V2 jsou vrcholy grafu, které mohou nabývat pouze hodnot "A-Z". Pokud načtený řádek neobsahuje text v tomto formátu je ze seznamu řádků odstraněn. Řádky ve správném formátu jsou uloženy do nového seznamu ve formátu "[V1,V2]", který je dále využíván v samotném zpracování grafu.

Dále jsou v každé hraně vrcholy seřazeny a jsou odstraněny případné duplicitní hrany, které by v samotném výpočtu zpomalovaly činnost programu. Výsledný seznam hran bez duplicit je také ještě seřazen.

Po nachystání seznamu s hranami je kontrolováno, zda je zadaný graf spojitý. Pokud by nebyl, nemá smysl hledat jeho stromy. Kotrola toho zda je graf spojitý se provádí iterativním způsobem. Začíná se tak, že se do seznamu dostupných vrcholů uloží jeden vrchol zadaného grafu. Dále se do seznamu dostupných vrcholů iterativně přidávají všechny další vrcholy, na které se dá dostat z aktuálně dostupných vrcholů. Iterování končí když se seznam nezvetší od poslední iterace. Pokud seznam dostupných vrcholů obsahuje všechny vrcholy přítomné v hranách grafu, tak je zadaný graf spojitý a vrcholy jsou poslány na další zpracování. Pokud graf není spojiný, tak se seznam vrcholů vyprázdní, aby se výpočet koster neprováděl.

Následuje samotné hledání všech koster, které zadaný graf obsahuje. Hledání všech koster grafu je prováděno hrubou silou (Brute-force) a to tak, že se postupně generují všechny kombinace zadaného seznamu hran a u každé vygenerováné kombinace se zkotroluje zda náhodou není kostrou a pokud ano, tak je uložena do seznamu nalezených koster. Aby kombinace hran (de facto vygenerovaný podgraf) mohla být kostra, musí splňovat následující věci: musí být spojitá, nesmí obsahovat cykly a musí obsahovat všechny vrcholy původního grafu. Kontrola toho zda je vygenerovaný graf spojitý je prováděna stejným způsobem jak už bylo výše zmíněno. Kontrola cyklů je prováděna podobným způsobem jako spojitost. Začne se od jednoho vybraného vrcholu a iterativně se získávají další dostupné vrcholy, na které jde přejít z aktuálně dostupných vrcholů. Graf obsahuje cyklus, pokud nějaký vrchol z nově vypočítaných vrcholů je již přítomný v seznamu dostupných vrcholů, což znamená že se do jednoho vrcholu dá dostat více způsoby a to je cyklus.

Nakonec je programem proveden výpis každé nalezené kostry ze seřazeného seznamu všech nalezených koster, ve kterém byly sežazeny i hrany v každé kostře. Výpis každé nalezené kostry je proveden ve formátu specifikovánem zadáním "V1-V2 V3-V4 ... Vn-1-Vn". Pokud je seznam nalezených koster prázdný (prázdný vstup nebo zadaný graf nebyl spojitý) tak se vypíše jen odsazení na další řádek.

Testy
---
K vytvořenému programu jsou ve složce <em>tests</em> přiloženy základní testy a skript v jazyce Python, který tyto testy spouští. Testy kontrolují zda dokáže vytvořený program správně odhalit vstupy se špatným formátem a pro zadané grafy nalézt všechny jejich kostry. Testy lze spustit přes program <em>make</em> příkazem <em>make test</em>. Před spuštěním testů je však potřeba mít samotný program přeložený.

Testovací skript byl spuštěn na serveru merlin a většina testovacích souborů trvá máximálně několik sekund. Déle trvají soubory <em>test13_in.txt</em> : 250 sekund a <em>test6_in.txt</em> : 205 sekund.