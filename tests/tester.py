# tester.py
# Logical projekt FLP, Spanning tree, 19.4.2022
# Author: Matej Kudera, xkuder04, VUT FIT
# Automated tester script

#### Imports ####
import subprocess
import os
import time

# Test actual directory
cwd = os.getcwd()
act_directory = cwd.split('/')[-1]

# Make relative path to test directory if needed
pwd_tests = ""
pwd_program = ""
if act_directory != "tests":
    pwd_tests += "tests/"
    pwd_program += "./flp21-log"
else:
    pwd_tests += "."
    pwd_program += "../flp21-log"

# Iterate through tests
passed_tests = 0
tests = 0
for file in os.listdir(pwd_tests):
    if file.endswith("_in.txt"):
        tests += 1

        # Get test files
        input_file = file
        output_file = file[:-7] + "_out.txt"
        print("Test: " + input_file)

        # Setup process
        pwd_input_file = os.path.join(pwd_tests, input_file)
        pwd_output_file = os.path.join(pwd_tests, output_file)

        bashCmd = [pwd_program]
        with open(pwd_input_file) as f: 
            process = subprocess.Popen(bashCmd, stdin=f, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            # Get start time 
            start_time = time.time() 

            # Run command and get result
            output, error = process.communicate()

        # Get run time
        run_time = time.time() - start_time
        print("Run time: " + str(run_time) + " seconds")

        expected_output = ""
        with open(pwd_output_file) as f:
            expected_output = f.read()

            if output == expected_output:
                print("Test passed")
                print("######################")
                passed_tests += 1
                continue

        print("Test failed")
        print("######################")

print("Passed "+str(passed_tests)+"/"+str(tests))

# END tester.py