# Makefile
# Logical project FLP, Spanning tree, 19.4.2022
# Author: Matej Kudera, xkuder04, VUT FIT
# Makefile for translation

######### Variables ########
PRG = swipl
FLAGS_PRG = --stand_alone=true

######## Build ########
all: flp21-log

flp21-log: src/Main.pl
	$(PRG) $(FLAGS_PRG) -o flp21-log -g main -c src/Main.pl

############ Comands ############
# Run tests
test:
	python tests/tester.py
	
# Clear repositary
clean:
	rm -f flp21-log flp-log-xkuder04.zip

# Make zip
zip: 
	zip flp-log-xkuder04.zip doc/* src/* tests/* Makefile

# END Makefile