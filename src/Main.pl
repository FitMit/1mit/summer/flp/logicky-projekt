% Main.pl
% Logical projekt FLP, Spanning tree, 19.4.2022
% Author: Matej Kudera, xkuder04, VUT FIT
% Main project file

% ----------------------------- Primary predicate which makes spanning trees for graph on stdin -----------------------------------------
main :-
        % Load input from stdin to internal representation
        prompt(_, ''),
		read_lines(Lines),

        % Each line consists of two characters separated with space (<V1> <V2>) and describes edge between two vertices
        % Vertices can by only characters A - Z
        % Lines with bad format are ignored
        get_edges(Lines, Loaded_graph_edges),

        % Remove duplicite edges and sort result
        sort_vertices_in_edges(Loaded_graph_edges, Sorted_graph_edges),
        remove_duplicite_edges(Sorted_graph_edges, Graph_edges_without_duplicities),
        sort(Graph_edges_without_duplicities, Graph_edges),

        % If graph is disconnected print empty result
        get_vertices(Graph_edges, Vertices_list),
        make_set(Vertices_list, Vertices_set),
        get_first_vertex(Graph_edges, First_vertex),
        get_reachable_vertices(Graph_edges, First_vertex, Reachable_vertices),
        check_connected(Reachable_vertices, Vertices_set, Graph_edges, Checked_graph),

        % Bruteforce every possible spanning tree in given graph
        % Remove duplicite spanning trees
        setof(N, make_spanning_tree(Checked_graph, N), Spanning_trees),
        sort_trees(Spanning_trees, Sorted_spanning_trees),
        sort(Sorted_spanning_trees, Sorted_list_of_sorted_spanning_trees),
        make_set(Sorted_list_of_sorted_spanning_trees, Final_spanning_trees),

        % Print each spanning tree on its own line in format <V1>-<V2> <V3>-<V4> ... <Vn-1>-<Vn> where Vn is vertex.
        % If result empty print only \n
		print_spanning_trees(Final_spanning_trees),
		halt.

% ----------------------------- Predicates for reading graph from stdin ------------------------------------------
% Reads line from stdin, terminates on LF or EOF.
% - line
% - readed character
read_line(L,C) :-
	get_char(C),
	(isEOFEOL(C), L = [], ! ; read_line(LL,_), L = [C|LL]).

% Tests if character is EOF or LF.
% - char
isEOFEOL(C) :-
	C == end_of_file;
	(char_code(C, Code), Code == 10).

% Reads all lines from stdin, terminates on EOF.
% - list of lines
read_lines(Ls) :-
	read_line(L, C),
	(C == end_of_file, Ls = [] ; read_lines(LLs), Ls = [L|LLs]).

% Get valid lines from loaded input
% - lines in sublists
% - result
get_edges([], []).
get_edges([[V1, ' ', V2]|T], R) :- 
    member(V1, ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']),
    member(V2, ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']),
    get_edges(T, RR),
    R = [[V1,V2]|RR].
get_edges([_|T], R) :- get_edges(T, R).

% Sort vertices in edge lists
% - edge list
% - result
sort_vertices_in_edges([],[]).
sort_vertices_in_edges([H|T], R) :- 
    sort_vertices_in_edges(T, RR), 
    sort(H, S), 
    R = [S|RR].

% Remove duplicities in loaded edges
% - edge list
% - result
remove_duplicite_edges([], []).
remove_duplicite_edges([H|T], R) :- 
    member(H, T) -> (remove_duplicite_edges(T, R)) ; (remove_duplicite_edges(T, RR), R = [H|RR]).

% ----------------------------- Check graph connected -------------------------------------------------
% Check if graph is connected and return edge list if true otherwise []
% - reachable vertices
% - all vertices
% - edge list
% - result
check_connected(RE, A, E, R) :-
    sort(RE, SRE),
    sort(A, SA),
    (SRE == SA -> (R = E) ; (R = [])).

% Get reachable vertices in graph
% - edge list
% - reachable vertices in actual iteration
% - result
get_reachable_vertices([], _, []).
get_reachable_vertices(E, O, R) :-
    get_vertices_adjacent(E, O, N),
    sort(N, SN),
    sort(O, SO),
    (SN == SO -> (R = SN) ; (get_reachable_vertices(E, SN, R))).

% Get adjacent vertices of given vertices
% - edge list
% - given vertex list
% - result
get_vertices_adjacent(_, [], []).
get_vertices_adjacent(E, [V|T], R) :-
    get_vertices_adjacent(E, T, RR),
    get_vertex_adjacent(E, V, RRR),
    append(RR, RRR, A),
    make_set(A, R).

% Get adjacent vertices of given vertex
% - edge list
% - given vertex
% - result
get_vertex_adjacent([], _, []).
get_vertex_adjacent([[V1, V2]|T], V, R) :-
    get_vertex_adjacent(T, V, RR),
    ((V1 == V ; V2 == V) -> (append([V1, V2], RR, A), make_set(A, R)) ; R = RR).

% Get first vertex in edge list
% - edge list
% - result
get_first_vertex([], []).
get_first_vertex([[V1,_]|_], R) :-
    R = [V1].

% Get all vertices in graph
% - edge list
% - result
get_vertices([], []).
get_vertices([[V1,V2]|T], R) :-
    get_vertices(T, RR),
    append([V1, V2], RR, R).

% Make set of vertices from list
% - vertices list
% - result
make_set([], []).
make_set([H|T], R) :-
    member(H,T) -> (make_set(T, R)) ; (make_set(T, RR), R = [H|RR]).

% ----------------------------- Spanning tree -------------------------------------------------
% Generate all combinations of list
% - input list
% - combination of list
get_list_combinations([],[]).
get_list_combinations([_|T],T2) :-
    get_list_combinations(T,T2).
get_list_combinations([H|T],[H|T2]) :-
    get_list_combinations(T,T2).

% Check spanning tree connected
% - edge list
% - all vertices
spanning_tree_connected(E, A):-
    get_first_vertex(E, FV),
    get_reachable_vertices(E, FV, RV),
    sort(RV, SRV),
    sort(A, SA),
    SA == SRV.

% Check if given graph is spanning tree
% - edge list
% - list of all vertices
is_spanning_tree(E, A) :-
    spanning_tree_connected(E, A),
    get_first_vertex(E, FV),
    has_no_loop(E, FV),
    get_vertices(E, LV),
    make_set(LV, SV),
    sort(A, SA),
    sort(SV, SSV),
    SA == SSV.

% Check if graph contains loop
% Break if graph is not connected
% - edge list
% - list of checked verticies in actual iteration
has_no_loop([], _).
has_no_loop(E, V) :-
    get_vertices_adjacent_for_loop(E, V, AD),
    is_set(AD),
    check_not_contains(AD, V),
    remove_edges_with_vertices(E, V, RE),
    E \= RE,
    append(V, AD, NV),
    has_no_loop(RE, NV).

% Get adjacent vertices of given vertices and dont include their starting vertices
% - edge list
% - given vertex list
% - result
get_vertices_adjacent_for_loop(_, [], []).
get_vertices_adjacent_for_loop(E, [V|T], R) :-
    get_vertices_adjacent_for_loop(E, T, RR),
    get_vertex_adjacent_for_loop(E, V, RRR),
    append(RR, RRR, R).

% Get adjacent vertices of vertex and dont include starting vertex
% - edge list
% - vertex
% - result
get_vertex_adjacent_for_loop([], _, []).
get_vertex_adjacent_for_loop([[V1, V2]|T], V, R) :-
    get_vertex_adjacent_for_loop(T, V, RR),
    ((V1 == V , V2 == V) -> (append([V1, V2], RR, R)) ; (V1 == V -> (append([V2], RR, R)) ; (V2 == V -> (append([V1], RR, R)) ; (R = RR)))).

% Remove edges containing vertices
% - edge list
% - given vertex list
% - result
remove_edges_with_vertices(E, [], E).
remove_edges_with_vertices(E, [V|T], R) :-
    remove_edges_with_vertex(E, V, RE),
    remove_edges_with_vertices(RE, T, R).

% Remove edges containing vertex
% - edge list
% - vertex
% - result
remove_edges_with_vertex([], _, []).
remove_edges_with_vertex([[V1,V2]|T], V, R) :-
    remove_edges_with_vertex(T, V, RR),
    ((V == V1 ; V == V2) -> (R = RR) ; (R = [[V1,V2]|RR])).

% Check if second list not contains any element from first list
% - first list
% - second list
check_not_contains([], _).
check_not_contains([H|T], L) :-
    not(member(H, L)),
    check_not_contains(T, L).

% Predicate making spanning tree
% - edge list
% - result
make_spanning_tree(E, R) :-
    get_list_combinations(E, EC),
    get_vertices(E, LV),
    make_set(LV, SV),
    is_spanning_tree(EC, SV),
    R = EC.

% Sort edges of each spanning tree in list of spanning trees
% - list of spanning trees
% - result
sort_trees([], []).
sort_trees([H|T], R) :-
    sort_trees(T, RR),
    sort(H, ST),
    R = [ST|RR].

% Print list of spanning trees
% - list of spanning trees
print_spanning_trees([[]]).
print_spanning_trees([]).
print_spanning_trees([H|T]) :-
    print_spanning_tree(H),
    write('\n'),
    print_spanning_trees(T).
    
% Print spanning tree in specified format
% - list containing edges of spanning tree
print_spanning_tree([]).
print_spanning_tree([[V1, V2]|T]) :-
    write(V1),
    write('-'),
    write(V2),
    (T == [] -> (print_spanning_tree(T)) ; (write(' '), print_spanning_tree(T))).
    
% END Main.pl